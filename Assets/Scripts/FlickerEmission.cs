﻿using UnityEngine;

/// <summary>
/// Flickers the emission value of a <see cref="Renderer"/>
/// </summary>
public class FlickerEmission : MonoBehaviour
{
	[SerializeField]
	protected float m_MinimumIntensity = 1f;

	[SerializeField]
	protected float m_MaximumIntensity = 2f;

	[SerializeField]
	protected float m_FlickerSmoothing = 0.5f;

	protected float m_Intensity;

	[SerializeField]
	protected Renderer[] m_Renderers;

	protected Material[] m_Materials;
	protected Color[] m_InitialColors;

	protected void Start()
	{
		m_Materials = new Material[m_Renderers.Length];
		m_InitialColors = new Color[m_Renderers.Length];
		for (int i = 0; i < m_Renderers.Length; i++)
		{
			m_Materials[i] = m_Renderers[i].material;
			m_InitialColors[i] = m_Materials[i].GetColor("_EmissionColor");
		}
	}

	protected void Update()
	{
		float randomIntensityMultiplier = Random.Range(m_MinimumIntensity, m_MaximumIntensity);
		m_Intensity = Mathf.Lerp(m_Intensity, randomIntensityMultiplier, m_FlickerSmoothing * Time.deltaTime);
		for (int i = 0; i < m_Renderers.Length; i++)
		{
			m_Materials[i].SetColor("_EmissionColor", m_InitialColors[i] * m_Intensity);
		}
	}
}