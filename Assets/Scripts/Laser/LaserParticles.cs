﻿using UnityEngine;

public class LaserParticles : MonoBehaviour
{
	public Camera raycastCamera;
	public ParticleSystem particleObject;
	public Transform laserObject;

	public float lerpTime = 50f;

	protected RaycastHit m_Hit;

	private bool IsOn
	{
		get
		{
			return laserObject.gameObject.activeInHierarchy;
		}
	}

	private void Start()
	{
	}

	private void LateUpdate()
	{
		if (Input.GetMouseButton(0))
		{
			if (!IsOn)
			{
				laserObject.gameObject.SetActive(true);
				particleObject.Play();
			}
			float lerpT = Time.deltaTime * lerpTime;
			Ray ray = raycastCamera.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out m_Hit))
			{
				Debug.DrawLine(ray.origin, m_Hit.point);
				Debug.Log(m_Hit.normal);
				particleObject.transform.rotation = Quaternion.FromToRotation(Vector3.up, m_Hit.normal);
				particleObject.transform.localPosition = m_Hit.point;
				laserObject.LookAt(m_Hit.point);
				laserObject.localScale = new Vector3(1, 1, m_Hit.distance);
			}
		}
		else
		{
			if (IsOn)
			{
				laserObject.gameObject.SetActive(false);
				particleObject.Stop(false, ParticleSystemStopBehavior.StopEmitting);
			}
		}
	}
}