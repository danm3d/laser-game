﻿using UnityEngine;

public class RefractiveSurface : LaserSurface
{
	[SerializeField]
	protected float m_IndexOfRefraction = 1f;

	public override Vector2 Reflect(Vector2 direction, Vector2 normal)
	{
		var refraction = VectorUtilities.Refract(1f, m_IndexOfRefraction, normal, direction);
		return refraction;
	}
}