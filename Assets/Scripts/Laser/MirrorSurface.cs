﻿using UnityEngine;

public class MirrorSurface : LaserSurface
{
	public override Vector2 Reflect(Vector2 direction, Vector2 normal)
	{
		return Vector2.Reflect(direction, normal);
	}
}