﻿using UnityEngine;

public class LaserEmitter : LineLaser
{
	[SerializeField]
	protected Transform m_FirePoint;

	protected override void Update()
	{
		base.Update();
		m_LineRenderer.SetPosition(0, m_FirePoint.position);
		Ray2D ray = new Ray2D(m_FirePoint.position, m_FirePoint.right);
		Emit(ray);
	}
}