﻿using UnityEngine;

/// <summary>
/// Base class for a Raycast based laser system
/// </summary>
public abstract class LaserBase : MonoBehaviour, ILaser
{
	/// <summary>
	/// Maximum distance that the Raycast can go
	/// </summary>
	[SerializeField]
	protected float m_MaximumDistance = 20f;

	/// <summary>
	/// Maximum number of reflections that the laser can make before it stops
	/// </summary>
	[SerializeField]
	protected int m_MaxContactPoints;

	/// <summary>
	/// Only Raycast on these layers
	/// </summary>
	[SerializeField]
	protected LayerMask m_ReflectionMask;

	/// <summary>
	/// Has the laser come into contact with a valid surface?
	/// </summary>
	protected bool m_HasContact;

	/// <summary>
	/// Current number of laser bounces
	/// </summary>
	protected int m_HitCount = 0;

	/// <summary>
	/// Cached <see cref="RaycastHit2D"/>
	/// </summary>
	protected RaycastHit2D m_Hit;

	protected virtual void Start()
	{
	}

	protected virtual void Update()
	{
		m_HitCount = 0; // Reset bounces each update
	}

	/// <summary>
	/// Casts a ray out to check if contact is made
	/// </summary>
	/// <param name="ray">The <see cref="Ray"/> that will be cast</param>
	public virtual void Emit(Ray2D ray)
	{
		m_HasContact = m_Hit = Physics2D.Raycast(ray.origin, ray.direction, m_MaximumDistance, m_ReflectionMask);

#if UNITY_EDITOR
		if (m_Hit)
		{
			Debug.DrawLine(ray.origin, m_Hit.point, Color.yellow);
			Debug.DrawRay(m_Hit.point, m_Hit.normal, Color.white);
		}
#endif
	}
}