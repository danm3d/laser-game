﻿using UnityEngine;

public interface ILaser
{
	void Emit(Ray2D ray);
}