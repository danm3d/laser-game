﻿using UnityEngine;

/// <summary>
/// Simple <see cref="LaserBase"/> implementation that uses a <see cref="LineRenderer"/> for visuals
/// </summary>
[RequireComponent(typeof(LineRenderer))]
public class LineLaser : LaserBase
{
	protected LineRenderer m_LineRenderer;

	protected override void Start()
	{
		base.Start();
		m_LineRenderer = GetComponent<LineRenderer>();
		if (m_LineRenderer != null)
		{
			m_LineRenderer.positionCount = m_MaxContactPoints;
		}
	}

	protected override void Update()
	{
		base.Update();
		m_LineRenderer.positionCount = 1;
	}

	public override void Emit(Ray2D ray)
	{
		base.Emit(ray);
		if (m_HasContact && m_HitCount < m_MaxContactPoints)
		{
			m_HitCount++;
			m_LineRenderer.positionCount = m_HitCount + 1;
			m_LineRenderer.SetPosition(m_HitCount, m_Hit.point);

			ILaserSurface surface = m_Hit.transform.GetComponent<ILaserSurface>();
			if (surface != null)
			{
				Vector2 reflectAngle = surface.Reflect(ray.direction, m_Hit.normal);
				Emit(new Ray2D(m_Hit.point, reflectAngle));
			}
		}
		else
		{
			m_LineRenderer.positionCount = m_HitCount + 2;
			m_LineRenderer.SetPosition(m_HitCount + 1, ray.GetPoint(m_MaximumDistance));
		}
	}
}