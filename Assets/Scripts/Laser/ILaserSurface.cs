﻿using UnityEngine;

public interface ILaserSurface
{
	Vector2 Reflect(Vector2 direction, Vector2 normal);
}