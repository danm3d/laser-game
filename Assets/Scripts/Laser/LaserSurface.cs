﻿using UnityEngine;

/// <summary>
/// Base class to handle different kinds of surfaces that lasers interact with
/// </summary>
public abstract class LaserSurface : MonoBehaviour, ILaserSurface
{
	public abstract Vector2 Reflect(Vector2 direction, Vector2 normal);
}