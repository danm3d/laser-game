﻿using UnityEngine;

public class SimpleRotator : MonoBehaviour
{
	[SerializeField]
	protected float m_RotationRate = 5f;

	protected float m_HorizontalInput;

	private void Start()
	{
	}

	private void Update()
	{
		m_HorizontalInput = Input.GetAxis("Horizontal");

		if (m_HorizontalInput != 0f)
		{
			transform.Rotate(Vector3.forward * m_HorizontalInput * -m_RotationRate * Time.deltaTime);
		}
	}
}