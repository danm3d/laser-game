﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PulseBloomEffect : MonoBehaviour
{
	[SerializeField]
	protected PostProcessVolume m_Volume;

	protected Bloom m_Bloom;

	[SerializeField]
	protected AnimationCurve m_AnimationCurve;

	[SerializeField]
	protected float m_Duration;

	[SerializeField]
	protected float m_IntensityMultiplier;

	protected float m_Timer;

	protected bool m_Success = false;

	protected void Start()
	{
		m_Success = m_Volume.profile.TryGetSettings(out m_Bloom);
		m_Timer = 0f;
	}

	private void Update()
	{
		if (m_Success)
		{
			m_Timer += Time.deltaTime;
			if (m_Timer >= m_Duration)
			{
				m_Timer = 0f;
			}

			m_Bloom.intensity.value = m_AnimationCurve.Evaluate(m_Timer / m_Duration) * m_IntensityMultiplier;
		}
	}
}