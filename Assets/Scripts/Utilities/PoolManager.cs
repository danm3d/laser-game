﻿using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
	/// <summary>
	/// A Pooling system to easily recycle objects
	/// </summary>
	public class PoolManager : Singleton<PoolManager>
	{
		[SerializeField]
		protected List<ObjectPool> m_Pools;

		public List<ObjectPool> Pools
		{
			get { return m_Pools; }
			set { m_Pools = value; }
		}

		protected Dictionary<string, Queue<GameObject>> m_PoolDictionary;

		/// <summary>
		/// Initialise the Pools on Start
		/// </summary>
		protected override void Awake()
		{
			base.Awake();
			m_PoolDictionary = new Dictionary<string, Queue<GameObject>>();
			foreach (ObjectPool pool in m_Pools)
			{
				Queue<GameObject> objectPool = new Queue<GameObject>();

				for (int i = 0; i < pool.size; i++)
				{
					GameObject obj = Instantiate(pool.prefab);
					obj.SetActive(false);
					objectPool.Enqueue(obj);
				}

				m_PoolDictionary.Add(pool.tag, objectPool);
			}
		}

		/// <summary>
		/// Take a pooled object with a given tag
		/// </summary>
		/// <param name="tag">The tag of the pool</param>
		public GameObject Take(string tag, Vector3 position, Quaternion rotation)
		{
			if (!m_PoolDictionary.ContainsKey(tag))
			{
				Debug.LogWarningFormat("Pool with Tag \"{0}\" does not exist.", tag);
				return null;
			}

			GameObject objectToSpawn = m_PoolDictionary[tag].Dequeue();
			objectToSpawn.SetActive(true);
			objectToSpawn.transform.position = position;
			objectToSpawn.transform.rotation = rotation;

			IPoolableObject pooledObject = objectToSpawn.GetComponent<IPoolableObject>();
			if (pooledObject != null)
			{
				pooledObject.OnObjectSpawn();
			}
			m_PoolDictionary[tag].Enqueue(objectToSpawn);

			return objectToSpawn;
		}
	}
}