﻿using UnityEngine;

namespace Utilities
{
	/// <summary>
	/// Data to populate a pool
	/// </summary>
	[System.Serializable]
	public class ObjectPool
	{
		public string tag;
		public GameObject prefab;
		public int size;
	}
}