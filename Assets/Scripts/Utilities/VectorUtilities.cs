﻿using UnityEngine;

public static class VectorUtilities
{
	public static Vector3 Refract(float RI1, float RI2, Vector3 surfaceNormal, Vector3 incidentDirection)
	{
		// Normalize both vectors to be sure
		surfaceNormal.Normalize();
		incidentDirection.Normalize();

		return (RI1 / RI2 * Vector3.Cross(surfaceNormal, Vector3.Cross(-surfaceNormal, incidentDirection)) - surfaceNormal * Mathf.Sqrt(1 - Vector3.Dot(Vector3.Cross(surfaceNormal, incidentDirection) * (RI1 / RI2 * RI1 / RI2), Vector3.Cross(surfaceNormal, incidentDirection)))).normalized;
	}
}