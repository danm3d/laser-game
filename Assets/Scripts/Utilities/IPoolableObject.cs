﻿namespace Utilities
{
	/// <summary>
	/// Interface for objects in a pool
	/// </summary>
	public interface IPoolableObject
	{
		/// <summary>
		/// Performs logic when the object is spawned from the pool
		/// </summary>
		void OnObjectSpawn();
	}
}