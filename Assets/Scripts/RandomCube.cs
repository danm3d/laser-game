﻿using UnityEngine;
using Utilities;

public class RandomCube : MonoBehaviour, IPoolableObject
{
	protected Rigidbody body;

	public float min = 0f;
	public float max = 2f;

	public void OnObjectSpawn()
	{
		body = GetComponent<Rigidbody>();
		body.velocity = Vector3.zero;

		float randomX = Random.Range(min, max);
		float randomY = Random.Range(min, max);
		float randomZ = Random.Range(min, max);

		body.AddForce(new Vector3(randomX, randomY, randomZ), ForceMode.Impulse);
	}
}