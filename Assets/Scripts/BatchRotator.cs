﻿using UnityEngine;

public class BatchRotator : MonoBehaviour
{
	[SerializeField]
	protected RotationConfiguration[] m_ObjectsToRotate;

	private void Start()
	{
	}

	private void Update()
	{
		for (int i = 0; i < m_ObjectsToRotate.Length; i++)
		{
			var obj = m_ObjectsToRotate[i];
			obj.objectTransform.Rotate(obj.objectTransform.forward, obj.speed * Time.deltaTime);
		}
	}
}

[System.Serializable]
public class RotationConfiguration
{
	public Transform objectTransform;
	public float speed;
}