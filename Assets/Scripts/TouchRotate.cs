﻿using UnityEngine;

public class TouchRotate : MonoBehaviour
{
	[SerializeField]
	protected float m_Smoothing = .3f;

	protected float m_Velocity;

	protected Vector2 m_WorldPosition;

	protected Camera m_RayCastCamera;

	protected float m_RotationZ;

	protected void Start()
	{
		Debug.Log("Touch To Rotate Initialised....");
	}

	protected void Update()
	{
		if (Input.GetMouseButton(0))
		{
			m_WorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector2 difference = m_WorldPosition - (Vector2)transform.position;

			difference.Normalize();

			m_RotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, 0f, m_RotationZ), m_Smoothing);
		}
	}
}