﻿using UnityEngine;
using Utilities;

public class CubeSpawner : MonoBehaviour
{
	protected PoolManager poolMgr;

	[SerializeField]
	protected float m_TimeBetweenSpawns = 1.0f;

	protected float m_Timer;

	private void Start()
	{
		m_Timer = m_TimeBetweenSpawns;
		poolMgr = PoolManager.Instance;
	}

	private void Update()
	{
		m_Timer -= Time.deltaTime;
		if (m_Timer < 0)
		{
			poolMgr.Take("Cube", Vector3.zero, Quaternion.identity);
			m_Timer = m_TimeBetweenSpawns;
		}
	}
}