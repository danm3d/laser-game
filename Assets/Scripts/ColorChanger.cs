﻿using UnityEngine;

public class ColorChanger : MonoBehaviour
{
	[SerializeField]
	protected float m_Duration = 1f;

	protected float m_Timer = 0f;

	[SerializeField]
	protected Gradient m_Gradient;

	[SerializeField]
	protected MeshRenderer m_Renderer;

	protected Material m_Material;

	[SerializeField]
	protected bool m_RandomizeStartTime = false;

	protected void Start()
	{
		if (m_Renderer != null)
		{
			m_Material = m_Renderer.material;
		}
		m_Timer = m_RandomizeStartTime ? Random.Range(0f, m_Duration) : 0f;
		Debug.LogFormat("Random Value: {0}", m_Timer);
	}

	protected void Update()
	{
		m_Timer -= Time.deltaTime;
		float normalisedTime = m_Timer / m_Duration;

		m_Material.color = m_Gradient.Evaluate(normalisedTime);
		if (m_Timer <= 0f)
		{
			m_Timer = m_Duration;
		}
	}
}